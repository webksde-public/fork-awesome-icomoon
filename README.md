# Fork Awesome Icomoon - SASS

This repo is only about providing a icomoon.io package of Fork Awesome.

## Known Issues

Some icons are not sized perfectly, because the source of this package where the broken SVG icons from the official Fork Awesome repo. I've basically fixed this "automatically" by using the centering and size fitting options of Icomoon.

Merge requests welcome!

## License
Fork Awesome 1.2.0 · A fork of Font Awesome, originally created by Dave Gandy, now maintained by a community. Fork Awesome is licensed under SIL OFL 1.1· Code is licensed under MIT License. Documentation is licensed under CC BY 3.0.

